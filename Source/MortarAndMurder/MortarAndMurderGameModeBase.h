// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MortarAndMurderGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MORTARANDMURDER_API AMortarAndMurderGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "Pickups/MAM_BasePickup.h"
#include "MAM_ShellsPickup.generated.h"

/**
 * 
 */
UCLASS()
class MORTARANDMURDER_API AMAM_ShellsPickup : public AMAM_BasePickup
{
	GENERATED_BODY()

protected:

	virtual bool GivePickUp(APawn* Pawn) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	int32 ProfitShells = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	float MinDistanceSpawn = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	float MaxDistanceSpawn = 1000.0f;
	
};

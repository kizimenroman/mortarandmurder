// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MAM_BasePickup.generated.h"

class USphereComponent;

UCLASS()
class MORTARANDMURDER_API AMAM_BasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMAM_BasePickup();

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

protected:

	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;
	virtual bool GivePickUp(APawn* Pawn);
	void GetRandomSpawnLocation(APawn* Pawn, float MinSpawn, float MaxSpawn);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	USphereComponent* CollisionSphere;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
	UStaticMeshComponent* Body;	

private:	

	void GetNewYawRotation();
	void PickUpWasTaken();
	void SpawnNewItem();

	float RespTime = 5.0f;
	FVector NewSpawnPos;
	FTimerHandle SpawnTimer;
};

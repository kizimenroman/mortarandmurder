// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AMA_AimingComponent.generated.h"

class UMAM_BarrelComponent;
class UMAM_TurretComponent;
class AMAM_ProjectileBall;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MORTARANDMURDER_API UAMA_AimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UAMA_AimingComponent();

	UFUNCTION(BlueprintCallable, Category = "AimingComponent")
	void Init(UMAM_BarrelComponent* BarrelToSet, UMAM_TurretComponent* TurretToSet);

	UFUNCTION()
	void Fire();

	UFUNCTION()
	void AimAt(FVector HitLocation);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire Settings")
	float VelocityProj = 6000.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Fire Settings")
	float RateOfFire = 3.0f;

	bool CanFire(){return bCanFire;}
	bool bAiming = false;

protected:

	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire Settings")
	TSubclassOf<AMAM_ProjectileBall> BP_ProjecileToSet;

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	

private:	
	
	UMAM_BarrelComponent* Barrel;
	UMAM_TurretComponent* Turret;
	FRotator AimDirection;
	FTimerHandle FireRateTimer;
	bool bCanFire = true;

	void MoveBarrelToward(FRotator AimDirectionLocal);
	void UnlockFire();
};

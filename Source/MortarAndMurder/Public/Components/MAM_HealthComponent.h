// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MAM_HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MORTARANDMURDER_API UMAM_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UMAM_HealthComponent();

	void ChangeHP(float ValueForChange);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	float CurrentHP;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
	float MaxHP = 100.0f;

protected:

	virtual void BeginPlay() override;
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

	UFUNCTION()
	void DamageCallBack(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser);
};

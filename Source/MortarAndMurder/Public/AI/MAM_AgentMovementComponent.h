// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "MAM_AgentMovementComponent.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnable))
class MORTARANDMURDER_API UMAM_AgentMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()
	
public:

	virtual void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;
};

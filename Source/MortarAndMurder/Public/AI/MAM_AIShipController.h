// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MAM_AIShipController.generated.h"

class UMAM_AIPerceptionComponent;
class UAMA_AimingComponent;

UCLASS()
class MORTARANDMURDER_API AMAM_AIShipController : public AAIController
{
	GENERATED_BODY()

public:

	AMAM_AIShipController();
	virtual void Tick(float DeltaTime) override;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Perception")
	UMAM_AIPerceptionComponent* AIPerception;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FName FocusOnKeyName = "EnemyKey";

	virtual void OnPossess(APawn* InPawn) override;

	virtual void BeginPlay() override;

private:

	UAMA_AimingComponent* AIAimingComp = nullptr;

	AActor* GetEnemyToFocus() const;
};

// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "Pawn/MAM_MyPawnShip.h"
#include "MAM_AIPawnShip.generated.h"

class UBehaviorTree;
class UMAM_AgentMovementComponent;

UCLASS()
class MORTARANDMURDER_API AMAM_AIPawnShip : public AMAM_MyPawnShip
{
	GENERATED_BODY()

public:

	AMAM_AIPawnShip(const FObjectInitializer& ObjInit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	UBehaviorTree* Tree;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	UMAM_AgentMovementComponent* MovemenComponent;
	
};

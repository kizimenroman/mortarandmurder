// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "MAM_ServiceFindEnemy.generated.h"

/**
 * 
 */
UCLASS()
class MORTARANDMURDER_API UMAM_ServiceFindEnemy : public UBTService
{
	GENERATED_BODY()
	
public:

	UMAM_ServiceFindEnemy();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector EnemyKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};

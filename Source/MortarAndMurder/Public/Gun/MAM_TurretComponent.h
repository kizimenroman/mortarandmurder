// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "MAM_TurretComponent.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class MORTARANDMURDER_API UMAM_TurretComponent : public UStaticMeshComponent
{
	GENERATED_BODY()
	

public:

	void TurnTower(float RelativeSpeed);

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gun")
	float MaxDegreesPerSeconds = 5.0f;
};

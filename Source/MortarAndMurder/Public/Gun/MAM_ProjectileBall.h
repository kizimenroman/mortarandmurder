// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MAM_ProjectileBall.generated.h"

class UProjectileMovementComponent;
class USphereComponent;
class URadialForceComponent;

UCLASS()
class MORTARANDMURDER_API AMAM_ProjectileBall : public AActor
{
	GENERATED_BODY()
	
public:	

	AMAM_ProjectileBall();

	void LaunchPT(float Speed);

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	UProjectileMovementComponent* ProjMovement;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CollisionMesh")
	USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Impact")
	URadialForceComponent* RadialForce = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Impact")
	float DestroyDelay = 0.2f;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Impact")
	//float ActivatePhysics= 0.7f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CollisionMesh")
	UStaticMeshComponent* Body = nullptr;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


private:

	FTimerHandle DestroyTimer;
	FTimerHandle RigidBodyTimer;

	void DestroyProjectile();
	//void ActivateRigidBody();
};

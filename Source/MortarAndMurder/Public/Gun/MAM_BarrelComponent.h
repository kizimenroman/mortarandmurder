// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "MAM_BarrelComponent.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class MORTARANDMURDER_API UMAM_BarrelComponent : public UStaticMeshComponent
{
	GENERATED_BODY()
	

public:

	void ElevateBarrel(float RelativeSpeed);

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gun")
	float MaxDegreeesPerSpeed = 10.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gun")
	float MaxAngleElevate = 40.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Gun")
	float MinAngleLowering = 10.0f;
};

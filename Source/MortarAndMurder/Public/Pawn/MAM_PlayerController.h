// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MAM_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MORTARANDMURDER_API AMAM_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	virtual void SetPawn(APawn* InPawn) override;
	virtual void Tick( float DeltaSeconds ) override;
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CrossHairLocation")
	float CrossHairX = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CrossHairLocation")
	float CrossHairY = 0.33f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CrossHairLocation")
	float RayTraceRange = 10000.0f;

private:

	void GetSightHitLocation(FVector& HitLocation) const;
	void GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const;
	void GetHitLocation(FVector LookDirection, FVector& HitLocation) const;
	void AimAtCrossHair();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MAM_CoreTypes.h"
#include "MAM_MyPawnShip.generated.h"

class USkeletalMeshComponent;
class UPawnMovementComponent;
class UCameraComponent;
class USpringArmComponent;
class UAMA_AimingComponent;
class UAnimInstance;
class UMAM_HealthComponent;

UCLASS()
class MORTARANDMURDER_API AMAM_MyPawnShip : public APawn
{
	GENERATED_BODY()

public:

	AMAM_MyPawnShip(const FObjectInitializer& ObjInit);

	virtual void Tick(float DeltaTime) override;
	void TryAddShells(int32 GotShells);
	void TurnRight(float Axis);
	virtual void FireCallback();
	void ShipIsDead();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Technical")
	float TurnAngle = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	UStaticMeshComponent* ShipBody = nullptr;

	UPROPERTY(BlueprintReadOnly)
	EMAM_FireingState FireState;	

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	USkeletalMeshComponent* Bes1 = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	USkeletalMeshComponent* Bes2 = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	USkeletalMeshComponent* Lever = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	USkeletalMeshComponent* Roule = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	UPawnMovementComponent* MovementComponent;

	UPROPERTY(EditDefaultsOnly, BLueprintReadWrite, Category = "Health")
	UMAM_HealthComponent* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship Setup")
	float FloatingShip = 18.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship Setup")
	float FloatingFallOff = 3000.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Wepon")
	int32 Shells = 10;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	UAnimMontage* BesLeverAction;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	UAnimMontage* LeverAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Aiming")
	UAMA_AimingComponent* AimingComp = nullptr;

	bool bLeverAction = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float TurnStrenght = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
	float TickForwardMoving = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float MaxWndSpeed = 320.0f;	

	virtual void BeginPlay() override;

private:	
		
	UAnimInstance* InstBes2;
	UAnimInstance* InstLever;
	FTimerHandle SinkTimer;
	float LastFireTime = 0.0f;
	float ReloadTime;
	bool IsAlive = true;

	void UpdateFireState();
	void UpdateLevitatePosition(float DeltaTime);
	FVector GetCorrectionForce(float DeltaTime);
	void AddFalloffFloat();
};

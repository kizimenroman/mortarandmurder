// Suck my ass

#pragma once

#include "CoreMinimal.h"
#include "Pawn/MAM_MyPawnShip.h"
#include "MAM_PlayerPawnShip.generated.h"

/**
 * 
 */
UCLASS()
class MORTARANDMURDER_API AMAM_PlayerPawnShip : public AMAM_MyPawnShip
{
	GENERATED_BODY()
	
	AMAM_PlayerPawnShip(const FObjectInitializer& ObjInit);

public:

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	USpringArmComponent* Spring = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ship Setup")
	UCameraComponent* Camera = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float SpeedZoom = 0.01;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float StepToCamZoom = 0.1;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float MinCameraZoom= 500.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float MaxCameraZoom= 500.0f;

private:

	FTimerHandle SpringTimer;
	float SpringDeltaTime = 0.0f;

	void AddNewLenghSpring();
	void SubNewLenghSpring();
	void UpdateSpringArm(float Axis);
	void LookRight(float Axis);
	void LookUp(float Axis);
};

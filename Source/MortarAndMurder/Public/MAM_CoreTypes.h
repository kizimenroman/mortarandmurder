#pragma once

#include "MAM_CoreTypes.generated.h"


UENUM(BlueprintType)
enum class EMAM_FireingState : uint8
{
	AimingNow,
	Reload,
	HoldAim,
	NoShells
};


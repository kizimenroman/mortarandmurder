// Suck my ass


#include "Components/SphereComponent.h"
#include "Pickups/MAM_BasePickup.h"

AMAM_BasePickup::AMAM_BasePickup()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>("CollisionSphere");
	CollisionSphere->SetSphereRadius(25.0f);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	RootComponent = CollisionSphere;

	Body = CreateDefaultSubobject<UStaticMeshComponent>("Body");
	Body->SetupAttachment(CollisionSphere);
}

void AMAM_BasePickup::BeginPlay()
{
	Super::BeginPlay();
}

void AMAM_BasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMAM_BasePickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
	UE_LOG(LogTemp,Warning,TEXT("Overlap"));
	const auto Pawn = Cast<APawn>(OtherActor);
	if(!Pawn) {return;}

	if (GivePickUp(Pawn))
	{
		PickUpWasTaken();
	}
}

bool AMAM_BasePickup::GivePickUp(APawn* Pawn)
{
	return false;
}

void AMAM_BasePickup::PickUpWasTaken()
{
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	Body->SetVisibility(false);
	GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &AMAM_BasePickup::SpawnNewItem, RespTime);
}

void AMAM_BasePickup::SpawnNewItem()
{
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	CollisionSphere->SetWorldLocation(NewSpawnPos);
	Body->SetVisibility(true);
}

void AMAM_BasePickup::GetRandomSpawnLocation(APawn* Pawn, float MinSpawn, float MaxSpawn)
{
	const auto StartPos = Pawn->GetActorLocation();
	const float RandNum = FMath::RandRange(MinSpawn, MaxSpawn);
	NewSpawnPos = FVector((StartPos.X + RandNum),(StartPos.Y + RandNum),400.0f);
}
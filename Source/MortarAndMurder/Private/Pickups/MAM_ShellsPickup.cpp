// Suck my ass


#include "Pawn/MAM_MyPawnShip.h"
#include "Pickups/MAM_ShellsPickup.h"

bool AMAM_ShellsPickup::GivePickUp(APawn* Pawn)
{
	const auto Player = Cast<AMAM_MyPawnShip>(Pawn);
	if(!Player) return false;

	Player->TryAddShells(ProfitShells);
	GetRandomSpawnLocation(Player, MinDistanceSpawn, MaxDistanceSpawn);
	return true;
}
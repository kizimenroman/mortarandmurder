// Suck my ass

#include "AI/MAM_AIPawnShip.h"
#include "DrawDebugHelpers.h"
#include "AI/MAM_AgentMovementComponent.h"

void UMAM_AgentMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	const auto AIShip = Cast<AMAM_AIPawnShip>(GetOwner());
	if(!AIShip) return;

	auto ShipForward = GetOwner()->GetActorForwardVector().GetSafeNormal();
	auto AiForwardIntantion = MoveVelocity.GetSafeNormal();
	//DrawDebugSphere(GetWorld(), MoveVelocity.GetSafeNormal(), 1200.0f, 16, FColor::Red, false, 5.0f, {}, 5.0f);

	auto TurnProduction = FVector::CrossProduct(ShipForward,AiForwardIntantion).Z;
	AIShip->TurnRight(TurnProduction);
	//UE_LOG(LogTemp, Warning, TEXT("TurnProduction %f"), TurnProduction);
}
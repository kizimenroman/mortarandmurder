// Suck my ass


#include "EnvironmentQuery/EnvQueryTypes.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "AI/Services/MAM_EnemyContext.h"

void UMAM_EnemyContext::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	const auto QueryOwner = Cast<AActor>(QueryInstance.Owner.Get());
	if(!QueryOwner) return;

	const auto BBoard = UAIBlueprintHelperLibrary::GetBlackboard(QueryOwner);
	if(!BBoard) return;

	const auto ContextActor = Cast<AActor>(BBoard->GetValueAsObject(EnemyActorKey));
	if(!ContextActor) return;

	UEnvQueryItemType_Actor::SetContextHelper(ContextData, ContextActor);
}
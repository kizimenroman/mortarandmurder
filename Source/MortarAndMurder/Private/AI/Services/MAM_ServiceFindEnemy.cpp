// Suck my ass


#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/MAM_AIPerceptionComponent.h"
#include "AI/Services/MAM_ServiceFindEnemy.h"

UMAM_ServiceFindEnemy::UMAM_ServiceFindEnemy()
{
	NodeName = "Find Enemy";
}

void UMAM_ServiceFindEnemy::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto BBoard = OwnerComp.GetBlackboardComponent();
	if(!BBoard) return;

	const auto Controller = OwnerComp.GetAIOwner();
	const auto Perception = Controller->FindComponentByClass<UMAM_AIPerceptionComponent>();
	
	if (Perception)
	{
		BBoard->SetValueAsObject(EnemyKey.SelectedKeyName, Perception->GetClosestEnemy());
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
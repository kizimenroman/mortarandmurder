// Suck my ass


#include "AI/MAM_AIPerceptionComponent.h"
#include "AI/MAM_AIPawnShip.h"
#include "Components/AMA_AimingComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/MAM_AIShipController.h"

AMAM_AIShipController::AMAM_AIShipController()
{
	AIPerception = CreateDefaultSubobject<UMAM_AIPerceptionComponent>("Perception");
	SetPerceptionComponent(*AIPerception);
}

void AMAM_AIShipController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	const auto Char = Cast<AMAM_AIPawnShip>(InPawn);
	if (Char)
	{
		RunBehaviorTree(Char->Tree);
	}
}

void AMAM_AIShipController::BeginPlay()
{
	Super::BeginPlay();
}

void AMAM_AIShipController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	const auto AimComponent = GetPawn()->FindComponentByClass<UAMA_AimingComponent>();
	const auto Char = Cast<AMAM_AIPawnShip>(GetPawn());
	if(AimComponent && Char)
	{
		if(!GetEnemyToFocus()) return;
		AimComponent->AimAt(GetEnemyToFocus()->GetActorLocation());
		if (Char->FireState == EMAM_FireingState::HoldAim)
		{
			Char->FireCallback();
		}
	}
}

AActor* AMAM_AIShipController::GetEnemyToFocus() const
{
	if(!GetBlackboardComponent()) return nullptr;

	return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
// Suck my ass

#include "AIController.h"
#include "Perception/AISense_Sight.h"
#include "AI/MAM_AIPerceptionComponent.h"


AActor* UMAM_AIPerceptionComponent::GetClosestEnemy() const
{
	TArray<AActor*> AllSightTargets;
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), AllSightTargets);
	if (AllSightTargets.Num() == 0)
	{
		return nullptr;
	}

	const auto Controller = Cast<AAIController>(GetOwner());
	if(!Controller) return nullptr;

	const auto PawnOwner = Controller->GetPawn();
	if(!PawnOwner) return nullptr;

	AActor* BestPawn = nullptr;
	float BestDistance = MAX_FLT;

	for (auto CurrentPawn : AllSightTargets)
	{
		const auto CurrentDistance = (PawnOwner->GetActorLocation() - CurrentPawn->GetActorLocation()).Size();
		if (CurrentDistance < BestDistance)
		{
			BestDistance = CurrentDistance;
			BestPawn = CurrentPawn;
		}
	}
	return BestPawn;
}

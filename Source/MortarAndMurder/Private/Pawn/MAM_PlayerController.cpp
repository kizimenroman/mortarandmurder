// Suck my ass

#include "Camera/PlayerCameraManager.h"
#include "Components/AMA_AimingComponent.h"
#include "Pawn/MAM_MyPawnShip.h"
#include "Pawn/MAM_PlayerController.h"

void AMAM_PlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto Ship = Cast<AMAM_MyPawnShip>(InPawn);
		if(!Ship) return;
	}
}

void AMAM_PlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	AimAtCrossHair();
}

void AMAM_PlayerController::AimAtCrossHair()
{
	if (!GetPawn()) { return; }

	auto AimingComponent = GetPawn()->FindComponentByClass<UAMA_AimingComponent>();
	if(!AimingComponent) return;

	FVector HitLocation;
	GetSightHitLocation(HitLocation);
	AimingComponent->AimAt(HitLocation);
}

void AMAM_PlayerController::GetSightHitLocation(FVector& HitLocation) const
{
	int32 ViewportX;
	int32 ViewportY;
	GetViewportSize(ViewportX, ViewportY);
	auto ScreenLocation = FVector2D(ViewportX * CrossHairX, ViewportY * CrossHairY);

	FVector LookDirection;
	GetLookDirection(ScreenLocation, LookDirection);
	GetHitLocation(LookDirection, HitLocation);
}

void AMAM_PlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const
{
	FVector CameraWorldLocation;
	DeprojectScreenPositionToWorld( ScreenLocation.X, ScreenLocation.Y, CameraWorldLocation, LookDirection);
}

void AMAM_PlayerController::GetHitLocation(FVector LookDirection, FVector& HitLocation) const
{
	FHitResult HitResult;

	auto AimingComponent = GetPawn()->FindComponentByClass<UAMA_AimingComponent>();
	if(!AimingComponent) { return; }

	auto StartLocation = PlayerCameraManager->GetCameraLocation();
	float EmptyRange = AimingComponent->VelocityProj;

	auto EndLocation = StartLocation + LookDirection * RayTraceRange;
	auto EmptyPoint = StartLocation + LookDirection * EmptyRange;

	if (GetWorld()->LineTraceSingleByChannel(
		HitResult,
		StartLocation,
		EndLocation,
		ECollisionChannel::ECC_Camera))
	{
		HitLocation = HitResult.Location;
	}
	else
	{
		HitLocation = EmptyPoint;
	}
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/MAM_MyPawnShip.h"
#include "Components/InputComponent.h"
#include "Components/AMA_AimingComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Animation/AnimInstance.h"
#include "DrawDebugHelpers.h"
#include "Components/MAM_HealthComponent.h"
#include "Components/SkeletalMeshComponent.h"

AMAM_MyPawnShip::AMAM_MyPawnShip(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
	PrimaryActorTick.bCanEverTick = true;

	ShipBody = CreateDefaultSubobject<UStaticMeshComponent>("Ship");
	ShipBody->SetSimulatePhysics(true);
	ShipBody->SetMassOverrideInKg(NAME_None, 300.0f);
	ShipBody->SetAngularDamping(10.0f);
	RootComponent = ShipBody;

	AimingComp = CreateDefaultSubobject<UAMA_AimingComponent>("Aiming Component");

	Bes1 = CreateDefaultSubobject<USkeletalMeshComponent>("Bes 1");
	Bes1->SetupAttachment(RootComponent);

	Bes2 = CreateDefaultSubobject<USkeletalMeshComponent>("Bes 2");
	Bes2->SetupAttachment(RootComponent);

	Lever = CreateDefaultSubobject<USkeletalMeshComponent>("Lever");
	Lever->SetupAttachment(RootComponent);

	Roule = CreateDefaultSubobject<USkeletalMeshComponent>("Roule");
	Roule->SetupAttachment(RootComponent);

	HealthComponent = CreateDefaultSubobject<UMAM_HealthComponent>("Health System");
	MovementComponent = CreateDefaultSubobject<UPawnMovementComponent>("Movement Component");
	
}

void AMAM_MyPawnShip::BeginPlay()
{
	Super::BeginPlay();	
	ReloadTime = AimingComp->RateOfFire;
}

void AMAM_MyPawnShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateFireState();
	UpdateLevitatePosition(DeltaTime);
}

void AMAM_MyPawnShip::TurnRight(float Axis)
{
	if(Axis == 0.0f) 
	{
		TurnAngle = 0.0f;	
		return;
	}

	TurnAngle = Axis;
	ShipBody->AddTorque(FVector(0.0f, 0.0 , TurnStrenght * Axis), NAME_None, true);
}

void AMAM_MyPawnShip::FireCallback()
{

	if (FireState != EMAM_FireingState::Reload && FireState != EMAM_FireingState::NoShells)
	{
		InstBes2 = Bes2->GetAnimInstance();
		InstLever = Lever->GetAnimInstance();
		InstBes2->Montage_Play(BesLeverAction, 1.5f);
		InstLever->Montage_Play(LeverAction, 1.5f);
		AimingComp->Fire();
		LastFireTime = GetWorld()->GetTimeSeconds();
		--Shells;
	}
}

void AMAM_MyPawnShip::TryAddShells(int32 GotShells)
{
	if (Shells == 10)
	{
		return;
	}
	else
	{
		Shells = FMath::Clamp<int32>(Shells + GotShells, 1, 10);
	}
}

void AMAM_MyPawnShip::UpdateFireState()
{
	if (!AimingComp->bAiming && Shells != 0 && (GetWorld()->GetTimeSeconds() - LastFireTime) > ReloadTime)
	{
		FireState = EMAM_FireingState::AimingNow;
	}
	else if ((GetWorld()->GetTimeSeconds() - LastFireTime) < ReloadTime && Shells != 0)
	{
		FireState = EMAM_FireingState::Reload;
	}
	else if (AimingComp->bAiming && Shells != 0 && (GetWorld()->GetTimeSeconds() - LastFireTime) > ReloadTime)
	{
		FireState = EMAM_FireingState::HoldAim;
	}
	else if (Shells == 0)
	{
		FireState = EMAM_FireingState::NoShells;
	}
}

void AMAM_MyPawnShip::UpdateLevitatePosition(float DeltaTime)
{
	//if(!IsAlive) return;

	// floating
	const float FallOff = ShipBody->GetComponentLocation().Z  * FloatingFallOff;
	const float EndValue =  (100000.0f * FloatingShip) - FallOff;
	ShipBody->AddForce(FVector(0.0f,0.0f,EndValue));

	// anti slide force
	if(!GetCorrectionForce(DeltaTime).IsNearlyZero(5.0f))
	{
		ShipBody->AddForce(GetCorrectionForce(DeltaTime));
	}

	const auto ForceForward = ShipBody->GetForwardVector();
	const auto ForceLocation = GetActorRotation().Vector();

	if((FMath::Abs(GetVelocity().X) + FMath::Abs(GetVelocity().Y)) > MaxWndSpeed)
	{ 
		//ShipBody->AddForce(-ForceLocation * TickForwardMoving);
		return; 
	}

	// move forward
	
	ShipBody->AddForce(ForceLocation * TickForwardMoving);
	
}

FVector AMAM_MyPawnShip::GetCorrectionForce(float DeltaTime)
{
	auto ShipSlide = FVector::DotProduct(GetVelocity(), GetActorRightVector());
	auto CorrectionAcceleration = -ShipSlide / DeltaTime * GetActorRightVector();

	return ShipBody->GetMass() * CorrectionAcceleration;
}

void AMAM_MyPawnShip::ShipIsDead()
{
	//IsAlive = false;
	if(!Controller) return;
	Controller->ChangeState(NAME_Spectating);
	GetWorld()->GetTimerManager().SetTimer(SinkTimer, this, &AMAM_MyPawnShip::AddFalloffFloat, 0.1, true, 0.0f);
	SetLifeSpan(25.0f);
}

void AMAM_MyPawnShip::AddFalloffFloat()
{
	if (FloatingShip <= 0.0f)
	{
		GetWorld()->GetTimerManager().ClearTimer(SinkTimer);
	}
	else
	{
		FloatingShip -= 2.0f;
	}
}
// Suck my ass

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Pawn/MAM_PlayerPawnShip.h"


AMAM_PlayerPawnShip::AMAM_PlayerPawnShip(const FObjectInitializer& ObjInit) : Super(ObjInit)
{

	Spring = CreateDefaultSubobject<USpringArmComponent>("Spring");
	Spring->SetupAttachment(RootComponent);
	Spring->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(Spring);
}

void AMAM_PlayerPawnShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("TurnRight", this, &AMAM_PlayerPawnShip::TurnRight);
	PlayerInputComponent->BindAxis("CameraAxisY", this, &AMAM_PlayerPawnShip::LookRight);
	PlayerInputComponent->BindAxis("CameraAxisX", this, &AMAM_PlayerPawnShip::LookUp);
	PlayerInputComponent->BindAxis("CameraLenght", this, &AMAM_PlayerPawnShip::UpdateSpringArm);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMAM_PlayerPawnShip::FireCallback);
}

void AMAM_PlayerPawnShip::UpdateSpringArm(float Axis)
{
	if (GetWorldTimerManager().IsTimerActive(SpringTimer) || Axis == 0.0f) { return; }	

	if(Axis > 0)
	{
		GetWorld()->GetTimerManager().SetTimer(SpringTimer, this, &AMAM_PlayerPawnShip::AddNewLenghSpring, SpeedZoom, true, 0.0f);
	}
	else
	{
		GetWorld()->GetTimerManager().SetTimer(SpringTimer, this, &AMAM_PlayerPawnShip::SubNewLenghSpring, SpeedZoom, true);
	}
}

void AMAM_PlayerPawnShip::AddNewLenghSpring()
{
	SpringDeltaTime += SpeedZoom;

	if(SpringDeltaTime < 0.5)
	{
		Spring->TargetArmLength = FMath::Clamp<float>((Spring->TargetArmLength + StepToCamZoom), MinCameraZoom, MaxCameraZoom);
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(SpringTimer);
		SpringDeltaTime = 0.0f;
	}
}

void AMAM_PlayerPawnShip::SubNewLenghSpring()
{
	SpringDeltaTime += SpeedZoom;

	if(SpringDeltaTime < 0.5)
	{
		Spring->TargetArmLength = FMath::Clamp<float>((Spring->TargetArmLength - StepToCamZoom), MinCameraZoom, MaxCameraZoom);
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(SpringTimer);
		SpringDeltaTime = 0.0f;
	}
}

void AMAM_PlayerPawnShip::LookRight(float Axis)
{
	if(!Axis || !Controller->IsLocalPlayerController()) return;

	APlayerController* const  PC = Cast<APlayerController>(Controller);
	PC->AddPitchInput(-Axis);
}

void AMAM_PlayerPawnShip::LookUp(float Axis)
{
	if(!Axis || !Controller->IsLocalPlayerController()) return;

	APlayerController* const  PC = Cast<APlayerController>(Controller);
	PC->AddYawInput(Axis);
}
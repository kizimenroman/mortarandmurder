// Suck my ass


#include "Gun/MAM_BarrelComponent.h"

void UMAM_BarrelComponent::ElevateBarrel(float RelativeSpeed)
{
	//if(!GetWorld() || RelativeSpeed == 0.0f) return;

	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1.0f, 1.0f);
	auto ElevationChange = RelativeSpeed * MaxDegreeesPerSpeed * GetWorld()->DeltaTimeSeconds;
	auto RawNewElevation = GetRelativeRotation().Pitch + ElevationChange;
	auto Elevation = FMath::Clamp<float>(RawNewElevation, MinAngleLowering, MaxAngleElevate);
	SetRelativeRotation(FRotator(Elevation, 0.0f, 0.0f));
	
}
// Suck my ass


#include "Gun/MAM_ProjectileBall.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Components/SphereComponent.h"

// Sets default values
AMAM_ProjectileBall::AMAM_ProjectileBall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>("Collision");
	CollisionSphere->SetSphereRadius(10.0f);
	CollisionSphere->SetNotifyRigidBodyCollision(true);
	RootComponent = CollisionSphere;

	Body = CreateDefaultSubobject<UStaticMeshComponent>("Body");
	Body->SetupAttachment(RootComponent);
	Body->SetNotifyRigidBodyCollision(false);

	RadialForce = CreateDefaultSubobject<URadialForceComponent>("Radial Force");
	RadialForce->AttachToComponent(CollisionSphere, FAttachmentTransformRules::KeepRelativeTransform);
	RadialForce->bAutoActivate = false;
	RadialForce->Radius = 200.0f;
	RadialForce->ImpulseStrength = 300000.0f;

	ProjMovement = CreateDefaultSubobject<UProjectileMovementComponent>("Movement Component");
}

void AMAM_ProjectileBall::BeginPlay()
{
	Super::BeginPlay();
	
	CollisionSphere->OnComponentHit.AddDynamic(this, &AMAM_ProjectileBall::OnHit);
}

void AMAM_ProjectileBall::LaunchPT(float Speed)
{
	ProjMovement->SetVelocityInLocalSpace(FVector::ForwardVector * Speed);
	ProjMovement->Activate();

	//GetWorld()->GetTimerManager().SetTimer(RigidBodyTimer, this, &AMAM_ProjectileBall::ActivateRigidBody, ActivatePhysics, false);
}

//void AMAM_ProjectileBall::ActivateRigidBody()
//{
//	CollisionSphere->SetSimulatePhysics(true);
//}

void AMAM_ProjectileBall::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	RadialForce->FireImpulse();
	CollisionSphere->DestroyComponent();
	Body->DestroyComponent();
	UGameplayStatics::ApplyRadialDamage(GetWorld(), 30.0f, Hit.Location, RadialForce->Radius, UDamageType::StaticClass(), {}, this, GetInstigatorController(), false);
	DrawDebugSphere(GetWorld(), Hit.Location, RadialForce->Radius, 16, FColor::Red, false, 10.0f, {}, 5.0f);

	GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &AMAM_ProjectileBall::DestroyProjectile, DestroyDelay, false);
}

void AMAM_ProjectileBall::DestroyProjectile()
{
	Destroy();
}
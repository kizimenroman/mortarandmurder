// Suck my ass


#include "Gun/MAM_TurretComponent.h"

void UMAM_TurretComponent::TurnTower(float RelativeSpeed)
{
	//if(!GetWorld() || RelativeSpeed == 0.0f) return;

	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1.0f, 1.0f);
	auto DeltaRotator = RelativeSpeed * MaxDegreesPerSeconds * GetWorld()->DeltaTimeSeconds;
	auto NewRotationTurret = GetRelativeRotation().Yaw + DeltaRotator;
	SetRelativeRotation(FRotator(0.0f, NewRotationTurret, 0.0f));
}
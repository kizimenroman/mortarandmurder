// Suck my ass

#include "Gun/MAM_TurretComponent.h"
#include "Pawn/MAM_MyPawnShip.h"
#include "Pawn/MAM_PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Gun/MAM_ProjectileBall.h"
#include "Gun/MAM_BarrelComponent.h"
#include "DrawDebugHelpers.h"
#include "Components/AMA_AimingComponent.h"


UAMA_AimingComponent::UAMA_AimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UAMA_AimingComponent::Init(UMAM_BarrelComponent* BarrelToSet, UMAM_TurretComponent* TurretToSet)
{
	Barrel = BarrelToSet;
	Turret = TurretToSet;
}

void UAMA_AimingComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UAMA_AimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UAMA_AimingComponent::Fire()
{
	if(!Barrel || !BP_ProjecileToSet || !bCanFire) return;

	bCanFire = false;
	const auto Projectile = GetWorld()->SpawnActor<AMAM_ProjectileBall>(
							BP_ProjecileToSet,
							Barrel->GetSocketLocation("MuzzleSocket"),
							Barrel->GetSocketRotation("MuzzleSocket"));

	if (Projectile)	{ Projectile->LaunchPT(VelocityProj); }
	GetWorld()->GetTimerManager().SetTimer(FireRateTimer, this, &UAMA_AimingComponent::UnlockFire, RateOfFire , false, RateOfFire);
}

void UAMA_AimingComponent::AimAt(FVector HitLocation)
{
	if (!Barrel) { return; }

	FVector OutLaunchVelocity;
	FVector MuzzleLoc = Barrel->GetSocketLocation("MuzzleSocket");

	bool bHaveAimLoc = UGameplayStatics::SuggestProjectileVelocity(
		this,
		OutLaunchVelocity,
		MuzzleLoc,
		HitLocation,
		VelocityProj,
		false,
		0.0f,
		0.0f,
		ESuggestProjVelocityTraceOption::DoNotTrace
	);
		AimDirection = OutLaunchVelocity.GetSafeNormal().Rotation();
		MoveBarrelToward(AimDirection);
}

void UAMA_AimingComponent::MoveBarrelToward(FRotator AimDirectionLocal)
{
	if(!Barrel || !Turret) { return; }

	auto BarrelRotation = Barrel->GetForwardVector().Rotation();
	auto Delta = AimDirectionLocal - BarrelRotation;

	bAiming = FMath::IsNearlyZero(Delta.Pitch, 2.0f) && FMath::IsNearlyZero(Delta.Yaw, 2.0f);

	Barrel->ElevateBarrel(Delta.Pitch);

	if (FMath::Abs(Delta.Yaw) < 180)
	{
		Turret->TurnTower(Delta.Yaw);
	}
	else
	{
		Turret->TurnTower(-Delta.Yaw);
	}
}

void UAMA_AimingComponent::UnlockFire()
{
	bCanFire = true;
	UE_LOG(LogTemp,Warning,TEXT("Unlock Fire"));
	GetWorld()->GetTimerManager().ClearTimer(FireRateTimer);
}
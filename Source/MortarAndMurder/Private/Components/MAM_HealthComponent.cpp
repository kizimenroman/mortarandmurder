// Suck my ass


#include "Pawn/MAM_MyPawnShip.h"
#include "Components/MAM_HealthComponent.h"

UMAM_HealthComponent::UMAM_HealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UMAM_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHP = MaxHP;

	const auto Owner = GetOwner();
	if (Owner)
	{
		//Owner->OnTakeAnyDamage.AddDynamic(this, &UMAM_HealthComponent::DamageCallBack);
		Owner->OnTakeRadialDamage.AddDynamic(this, &UMAM_HealthComponent::DamageCallBack);
	}
}

//void UMAM_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//}

void UMAM_HealthComponent::DamageCallBack(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser )
{
	ChangeHP(-Damage);
	UE_LOG(LogTemp,Warning, TEXT("OnTake"));
}

void UMAM_HealthComponent::ChangeHP(float ValueForChange)
{
	if(ValueForChange == 0.0f) return;

	CurrentHP = FMath::Clamp<float>((CurrentHP + ValueForChange), 0.0f, MaxHP);

	if (FMath::IsNearlyZero(CurrentHP))
	{
		const auto Owner = Cast<AMAM_MyPawnShip>(GetOwner());
		if (Owner)
		{
			Owner->ShipIsDead();
		}
	}
}